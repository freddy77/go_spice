package main

/* see
 * https://stackoverflow.com/questions/13660864/reading-specific-number-of-bytes-from-a-buffered-reader-in-golang
 * for read specific bytes
 *
 * see
 * https://golang.org/pkg/encoding/binary/#Read
 * for parsing binaries
 */

import (
	"fmt"
	"io"
	"net"
	"os"
	"bytes"
	"encoding/binary"
	"crypto/rsa"
	"crypto/rand"
	"crypto/sha1"
	"crypto/tls"
	"encoding/asn1"
	"github.com/hajimehoshi/oto"
	"go_spice/spice"
)

const (
	HOST = "localhost"
	PORT = "5901"
)

func main() {

	err := spice_session(HOST, PORT, 0)
	exit_on_error(err)
}

type Session struct {
	host, port string
	connection_id uint32
}

type Channel interface {
	Handle_messages(hdr spice.SpiceMiniDataHeader, b []byte) error
}

type BaseChannel struct {
	funcs Channel
	session *Session
	conn net.Conn
	message_window, message_ack_count uint32
}

// TODO: bound checking... return error instead of paniching
func spice_session(host, port string, connection_id uint32) error {
	session := Session{host, port, connection_id}
	return spice_channel(&session, spice.SPICE_CHANNEL_MAIN, 0)
}

func spice_channel(session *Session, channel_type, channel_id uint8) error {

	var channel *BaseChannel
	switch (channel_type) {
	case spice.SPICE_CHANNEL_MAIN:
		c := MainChannel{BaseChannel:BaseChannel{session:session}}
		c.funcs = &c
		channel = &c.BaseChannel
	case spice.SPICE_CHANNEL_INPUTS:
		c := InputsChannel{BaseChannel:BaseChannel{session:session}}
		c.funcs = &c
		channel = &c.BaseChannel
	case spice.SPICE_CHANNEL_PLAYBACK:
		c := PlaybackChannel{BaseChannel:BaseChannel{session:session}}
		c.funcs = &c
		channel = &c.BaseChannel
	}

	err := channel.connect(channel_type, channel_id)
	if err != nil {
		return err
	}

	// handle messages
	conn := channel.conn
	for {
		var hdr spice.SpiceMiniDataHeader
		err = binary.Read(conn, binary.LittleEndian, &hdr)
		if err != nil {
			return err
		}
//		fmt.Println(channel_type, hdr)

		b := make([]byte, hdr.Size)
		_, err = io.ReadFull(conn, b)
		if err != nil {
			return err
		}

		// handle ACKs
		if (channel.message_ack_count != 0) {
			channel.message_ack_count--
			if (channel.message_ack_count == 0) {
				channel.write_header(spice.SPICE_MSGC_ACK, 0)
				channel.message_ack_count = channel.message_window
			}
		}

		err = channel.funcs.Handle_messages(hdr, b)
		if err != nil {
			return err
		}
	}

	return nil
}

func (channel *BaseChannel) connect(channel_type, channel_id uint8) error {
	session := channel.session

	// connect to server
	conn, err := tls.Dial("tcp", session.host + ":" + session.port, &tls.Config{InsecureSkipVerify:true})
	if err != nil {
		return err
	}
	defer func() {
		if conn != nil {
			conn.Close()
		}
	}()

	channel.conn = conn

	// prepare link message
	buf := new(bytes.Buffer)
	hdr := spice.SpiceLinkHeader{spice.SPICE_MAGIC, spice.SPICE_VERSION_MAJOR, spice.SPICE_VERSION_MINOR, 0}
	err = binary.Write(buf, binary.LittleEndian, hdr)
	if err != nil {
		return err
	}
	var common_caps, caps spice.Capabilities
	common_caps.Set(spice.SPICE_COMMON_CAP_PROTOCOL_AUTH_SELECTION, true)
	common_caps.Set(spice.SPICE_COMMON_CAP_MINI_HEADER, true)
	//common_caps.Set(spice.SPICE_COMMON_CAP_AUTH_SASL, true)
	msg := spice.SpiceLinkMess{session.connection_id, channel_type, channel_id, common_caps.Len(), caps.Len(), 0}
	if err = binary.Write(buf, binary.LittleEndian, msg); err != nil {
		return err
	}
	b := buf.Bytes()
	binary.LittleEndian.PutUint32(b[len(b)-4:], uint32(len(b) - 16))
	buf.Write(common_caps.Bytes())
	buf.Write(caps.Bytes())
	b = buf.Bytes()
	binary.LittleEndian.PutUint32(b[12:], uint32(len(b) - 16))

	// send buffer to server
	conn.Write(buf.Bytes())

	// receive header from server
	err = binary.Read(conn, binary.LittleEndian, &hdr)
	if err != nil {
		return err
	}
	// check header returned (versions, magic, size)
	if hdr.Magic != spice.SPICE_MAGIC {
		return fmt.Errorf("Wrong SPICE header magic %x", hdr.Magic)
	}
	if hdr.Size > 4096 {
		return fmt.Errorf("SPICE header too big %d", hdr.Size)
	}
	if hdr.Major != spice.SPICE_VERSION_MAJOR {
		return fmt.Errorf("SPICE header mismatch %d.%d", hdr.Major, hdr.Minor)
	}

	// read reply from server
	b = make([]uint8, hdr.Size)
	_, err = io.ReadFull(conn, b)
	if err != nil {
		return err
	}
	var reply spice.SpiceLinkReply
	err = binary.Read(bytes.NewReader(b), binary.LittleEndian, &reply)
	if err != nil {
		return err
	}
	off := reply.Caps_offset
	if off > uint32(len(b)) || reply.Num_common_caps > 1024 || reply.Num_channel_caps > 1024 ||
		off + (reply.Num_common_caps + reply.Num_channel_caps) * 4 > uint32(len(b)) {
		return fmt.Errorf("SPICE reply overflow detected")
	}
	common_caps = spice.NewCapabilitiesFromBytes(b[off:off + reply.Num_common_caps * 4])
	off += reply.Num_common_caps * 4
	caps = spice.NewCapabilitiesFromBytes(b[off:off + reply.Num_channel_caps *4])

	// send SPICE auth (if caps set)
	if common_caps.Get(spice.SPICE_COMMON_CAP_AUTH_SPICE) {
		err = binary.Write(conn, binary.LittleEndian, uint32(spice.SPICE_COMMON_CAP_AUTH_SPICE))
		if err != nil {
			return err
		}

		pub_rsa, err := decode_pub_key(reply.Pub_key[:])
		if err != nil {
			return err
		}
		// TODO proper password (include C terminator)
		pwd, err := rsa.EncryptOAEP(sha1.New(), rand.Reader, pub_rsa, []byte{0}, nil)
		if err != nil {
			return err
		}
		_, err = conn.Write(pwd)
		if err != nil {
			return err
		}

		var res uint32
		err = binary.Read(conn, binary.LittleEndian, &res)
		if err != nil {
			return err
		}
		// TODO check it
		fmt.Println("Connection error", res)
		if res != 0 {
			return fmt.Errorf("Authentication error %d", res)
		}
	} else {
		// TODO other auth support
		return fmt.Errorf("Unsupported authentication schema")
	}

	if !common_caps.Get(spice.SPICE_COMMON_CAP_MINI_HEADER) {
		// TODO support
		return fmt.Errorf("Large headers not supported")
	}

	conn = nil

	// TODO return conn
	return nil
}

func exit_on_error(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func (c *BaseChannel) write_header(msg_type uint16, size uint32) error {
	hdr := spice.SpiceMiniDataHeader{msg_type, size}
	return binary.Write(c.conn, binary.LittleEndian, hdr)
}

func (c *BaseChannel) Handle_messages(hdr spice.SpiceMiniDataHeader, b []byte) error {
	switch hdr.Type {
	case spice.SPICE_MSG_PING:
		// ping
/*
		var ping struct {
			Id uint32
			Timestamp uint64
		}
		err = binary.Read(bytes.NewReader(b), binary.LittleEndian, &ping)
*/
		c.write_header(spice.SPICE_MSGC_PONG, 12)
		c.conn.Write(b[:12])
	case spice.SPICE_MSG_SET_ACK:
		var set_ack struct {
			Generation, Window uint32
		}
		binary.Read(bytes.NewReader(b), binary.LittleEndian, &set_ack)
		c.message_window = set_ack.Window
		c.message_ack_count = set_ack.Window
		c.write_header(spice.SPICE_MSGC_ACK_SYNC, 4)
		binary.Write(c.conn, binary.LittleEndian, set_ack.Generation)
	}
	return nil
}

type MainChannel struct {
	BaseChannel
}

func (c *MainChannel) Handle_messages(hdr spice.SpiceMiniDataHeader, b []byte) error {
	switch hdr.Type {
	case spice.SPICE_MSG_MAIN_INIT:
		binary.Read(bytes.NewReader(b), binary.LittleEndian, &c.session.connection_id)

		c.write_header(spice.SPICE_MSGC_MAIN_ATTACH_CHANNELS, 0)
	case spice.SPICE_MSG_MAIN_CHANNELS_LIST:
		fmt.Println(b)
		for i := 4; i +2 <= len(b); i += 2 {
			if b[i] == spice.SPICE_CHANNEL_INPUTS && b[i+1] == 0 {
				// got the input channel !!
				fmt.Println("channel", b[i], b[i+1])
				go spice_channel(c.session, b[i], b[i+1])
			}
			if b[i] == spice.SPICE_CHANNEL_PLAYBACK && b[i+1] == 0 {
				// got the input channel !!
				fmt.Println("channel", b[i], b[i+1])
				go spice_channel(c.session, b[i], b[i+1])
			}
		}
	default:
		return c.BaseChannel.Handle_messages(hdr, b)
	}
	return nil
}

type InputsChannel struct {
	BaseChannel
}

func (c *InputsChannel) Handle_messages(hdr spice.SpiceMiniDataHeader, b []byte) error {
	switch hdr.Type {
	case spice.SPICE_MSG_INPUTS_INIT:
		// init
		b := []byte{0x1e,0x9e,0x1f,0x9f,0x20,0xa0}
		c.write_header(spice.SPICE_MSGC_INPUTS_KEY_SCANCODE, uint32(len(b)))
		c.conn.Write(b)
	case spice.SPICE_MSG_INPUTS_KEY_MODIFIERS:
	case spice.SPICE_MSG_INPUTS_MOUSE_MOTION_ACK:
	default:
		return c.BaseChannel.Handle_messages(hdr, b)
	}
	return nil
}

type PlaybackChannel struct {
	BaseChannel
	oto_ctx *oto.Context
	player *oto.Player
}

func (c *PlaybackChannel) close_sound() {
	if c.player != nil {
		c.player.Close()
		c.player = nil
	}
	if c.oto_ctx != nil {
		c.oto_ctx.Close()
		c.oto_ctx = nil
	}
}

func (c *PlaybackChannel) Handle_messages(hdr spice.SpiceMiniDataHeader, b []byte) error {
	switch hdr.Type {
	case spice.SPICE_MSG_PLAYBACK_DATA:
		if c.player != nil {
			c.player.Write(b[4:])
		}
	case spice.SPICE_MSG_PLAYBACK_MODE:
	case spice.SPICE_MSG_PLAYBACK_START:
		var start struct {
			Channels uint32
			Format uint16
			Frequency uint32
			Time uint32
		}
		fmt.Println(b)
		err := binary.Read(bytes.NewReader(b), binary.LittleEndian, &start)
		if err != nil { return err }
		c.close_sound()
		// TODO test format
		c.oto_ctx, err = oto.NewContext(int(start.Frequency), int(start.Channels), 2, 4096)
		if err != nil { return err }
		c.player = c.oto_ctx.NewPlayer()
	case spice.SPICE_MSG_PLAYBACK_STOP:
		c.close_sound()
	case spice.SPICE_MSG_PLAYBACK_VOLUME:
	case spice.SPICE_MSG_PLAYBACK_MUTE:
	case spice.SPICE_MSG_PLAYBACK_LATENCY:
	default:
		return c.BaseChannel.Handle_messages(hdr, b)
	}
	return nil
}


func decode_pub_key(pk []byte) (*rsa.PublicKey, error) {
	// decode type and binary data
	var pub_key struct {
		O struct {
			O asn1.ObjectIdentifier
		}
		B asn1.BitString
	}
	_, err := asn1.Unmarshal(pk, &pub_key)
	if err != nil {
		return nil, err
	}

	// check OID, should be an RSA
	rsa_oid := asn1.ObjectIdentifier{1,2,840,113549,1,1,1}
	if !rsa_oid.Equal(pub_key.O.O) {
		return nil, fmt.Errorf("Expected only RSA key")
	}

	// now decode binary data
	var pub_rsa rsa.PublicKey
	_, err = asn1.Unmarshal(pub_key.B.Bytes, &pub_rsa)
	if err != nil {
		return nil, err
	}
	return &pub_rsa, nil
}
