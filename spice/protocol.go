// from protocol.h

package spice

const (
	SPICE_MAGIC = 0x51444552

	SPICE_VERSION_MAJOR = 2
	SPICE_VERSION_MINOR = 2

	SPICE_TICKET_KEY_PAIR_LENGTH = 1024
	SPICE_TICKET_PUBKEY_BYTES = SPICE_TICKET_KEY_PAIR_LENGTH / 8 + 34
)

const (
	SPICE_COMMON_CAP_PROTOCOL_AUTH_SELECTION = iota
	SPICE_COMMON_CAP_AUTH_SPICE
	SPICE_COMMON_CAP_AUTH_SASL
	SPICE_COMMON_CAP_MINI_HEADER
)

const (
	SPICE_PLAYBACK_CAP_CELT_0_5_1 = iota
	SPICE_PLAYBACK_CAP_VOLUME
	SPICE_PLAYBACK_CAP_LATENCY
	SPICE_PLAYBACK_CAP_OPUS
)

const (
	SPICE_RECORD_CAP_CELT_0_5_1 = iota
	SPICE_RECORD_CAP_VOLUME
	SPICE_RECORD_CAP_OPUS
)

const (
	SPICE_MAIN_CAP_SEMI_SEAMLESS_MIGRATE = iota
	SPICE_MAIN_CAP_NAME_AND_UUID
	SPICE_MAIN_CAP_AGENT_CONNECTED_TOKENS
	SPICE_MAIN_CAP_SEAMLESS_MIGRATE
)

const (
	SPICE_DISPLAY_CAP_SIZED_STREAM = iota
	SPICE_DISPLAY_CAP_MONITORS_CONFIG
	SPICE_DISPLAY_CAP_COMPOSITE
	SPICE_DISPLAY_CAP_A8_SURFACE
	SPICE_DISPLAY_CAP_STREAM_REPORT
	SPICE_DISPLAY_CAP_LZ4_COMPRESSION
	SPICE_DISPLAY_CAP_PREF_COMPRESSION
	SPICE_DISPLAY_CAP_GL_SCANOUT
	SPICE_DISPLAY_CAP_MULTI_CODEC
	SPICE_DISPLAY_CAP_CODEC_MJPEG
	SPICE_DISPLAY_CAP_CODEC_VP8
	SPICE_DISPLAY_CAP_CODEC_H264
	SPICE_DISPLAY_CAP_PREF_VIDEO_CODEC_TYPE
	SPICE_DISPLAY_CAP_CODEC_VP9
	SPICE_DISPLAY_CAP_CODEC_H265
	SPICE_DISPLAY_CAP_STREAM_PARTIAL
)

const (
	SPICE_INPUTS_CAP_KEY_SCANCODE = iota
)

const (
	SPICE_SPICEVMC_CAP_DATA_COMPRESS_LZ4 = iota
)

const (
	SPICE_PORT_EVENT_OPENED = 0
	SPICE_PORT_EVENT_CLOSED = 1
	SPICE_PORT_EVENT_BREAK = 2
)

type SpiceLinkHeader struct {
	Magic uint32 // SPICE_MAGIC
	Major, Minor uint32 // 2, 2
	Size uint32 // size of all the rest
}

// to server
// Max 4K in SPICE server
type SpiceLinkMess struct {
	Connection_id uint32
	Channel_type uint8
	Channel_id uint8
	Num_common_caps uint32
	Num_channel_caps uint32
	Caps_offset uint32 // bytes from SpiceLinkMess
}

// from server
type SpiceLinkReply struct {
	Error uint32
	Pub_key [SPICE_TICKET_PUBKEY_BYTES]uint8
	Num_common_caps uint32
	Num_channel_caps uint32
	Caps_offset uint32
}

type SpiceLinkEncryptedTicket struct {
	Encrypted_data [SPICE_TICKET_KEY_PAIR_LENGTH / 8]byte
}

// to server
type SpiceLinkAuthMechanism  struct {
	Auth_mechanism uint32
}

type SpiceDataHeader struct {
	Serial uint64
	Type uint16
	Size uint32
	Sub_list uint32 //offset to SpiceSubMessageList[]
}

type SpiceMiniDataHeader struct {
	Type uint16
	Size uint32
}

type SpiceSubMessage struct {
	Type uint16
	Size uint32
}

type SpiceSubMessageList struct {
	Size uint16
	// Sub_messages []uint32; //offsets to SpicedSubMessage
}
