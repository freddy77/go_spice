// from protocol.h

package spice

import (
	"testing"
	"fmt"
)

func Test(t *testing.T) {
	var c Capabilities

	if c.Len() != 0 {
		t.Errorf("Initial capability should be 0")
	}

	if fmt.Sprintf("%x", c.Bytes()) != "" {
		t.Errorf("Initial capability should empty")
	}

	tests := []struct {
		idx uint32
		value bool
		output string
		msg string
	} {
		{ 2, false, "", "Setting false should not extend" },
		{ 2, true, "04000000", "Setting true should extend" },
		{ 65, false, "04000000", "Setting false error" },
		{ 65, true, "040000000000000002000000", "Error extending" },
		{ 65, false, "04000000", "Error shrinking" },
	}

	for _, i := range tests {
		c.Set(i.idx, i.value)
		if fmt.Sprintf("%x", c.Bytes()) != i.output {
			t.Errorf(i.msg)
		}
	}

	d := NewCapabilitiesFromBytes(c.Bytes())
	if fmt.Sprintf("%x", d.Bytes()) != "04000000" {
		t.Errorf("Error copying")
	}
}
