// from protocol.h

package spice

import (
	"bytes"
	"encoding/binary"
)

type Capabilities struct {
	caps []uint32
}

func NewCapabilitiesFromBytes(b []uint8) Capabilities {
	var caps []uint32
	for i := 0; i + 4 <= len(b); i += 4 {
		caps = append(caps, binary.LittleEndian.Uint32(b[i:]))
	}
	return Capabilities{caps}
}

func (c *Capabilities) Get(idx uint32) bool {
	if int(idx / 32) >= len(c.caps) {
		return false
	}
	return ((c.caps[idx / 32] >> (idx % 32)) & 1) != 0
}

func (c *Capabilities) Set(idx uint32, value bool) {
	if int(idx / 32) < len(c.caps) {
		if value {
			c.caps[idx / 32] |= 1 << (idx % 32)
		} else {
			c.caps[idx / 32] &= ^uint32(1 << (idx % 32))
			for len(c.caps) > 0 && c.caps[len(c.caps)-1] == 0 {
				c.caps = c.caps[:len(c.caps)-1]
			}
		}
	} else if value {
		for int(idx / 32) >= len(c.caps) {
			c.caps = append(c.caps, 0)
		}
		c.caps[idx / 32] |= 1 << (idx % 32)
	}
}

func (c *Capabilities) Bytes() []uint8 {
	buf := new(bytes.Buffer)
	for _, v := range c.caps {
		_ = binary.Write(buf, binary.LittleEndian, v)
	}
	return buf.Bytes()
}

func (c *Capabilities) Len() uint32 {
	return uint32(len(c.caps))
}
